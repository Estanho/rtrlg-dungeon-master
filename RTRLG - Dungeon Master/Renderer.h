﻿#pragma once

#include <wrl\client.h>
#include <memory>
#include <list>

#include "Direct3DBase.h"
#include <DirectXMath.h>

#include "SpriteBatch.h"
#include "SpriteFont.h"

#include "SpriteBase.h"
#include "GameFont.h"

#include "MapTile.h"

#include "TextureDatabase.h"

// This class renders a simple spinning cube.
ref class Renderer sealed : public Direct3DBase
{
public:
	Renderer();

	// Direct3DBase methods.
	virtual void CreateDeviceResources() override;
	virtual void CreateWindowSizeDependentResources() override;
	virtual void Render() override;
	
	// Method for updating time-dependent objects.
	void Update(float timeTotal, float timeDelta);

	bool load();

private:
	bool m_loadingComplete;
	bool m_loadingComplete2;

	uint32 m_indexCount;

	unique_ptr<SpriteBatch> m_spriteBatch;
	unique_ptr<SpriteFont> arialFont16;
	unique_ptr<SpriteFont> arialFont50;
	unique_ptr<SpriteFont> timesFont16;
	
	list<SpriteBase*> sprites;
	list<GameFont*> texts;

	TextureDatabase* m_textures;

	MapTile gameMap[500][500];

	float upsideLeftPosX;
	float upsideLeftPosY;

	float scale;
};