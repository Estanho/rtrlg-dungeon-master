#include "pch.h"
#include "SpriteBase.h"

using namespace Windows::Graphics::Display;
using namespace DirectX;
using namespace Microsoft::WRL;

SpriteBase::SpriteBase() {
	throw;
}

SpriteBase::SpriteBase(float x, float y) {
	X = x;
	Y = y;
	m_Texture = nullptr;
}

void SpriteBase::setTexture(TextureDatabase* db, string text_name) {
	m_Texture = db->getTexture(text_name);
}

void SpriteBase::Draw(SpriteBatch* sb, Windows::Foundation::Rect wb) {

	float scale = DisplayProperties::LogicalDpi;
	float diameter = wb.Width * scale;
	float h = wb.Height * scale;
	sb->Draw(m_Texture.Get(), XMFLOAT2(X, Y), nullptr, Colors::White, 0.0f, XMFLOAT2(0.0f,0.0f), XMFLOAT2(diameter / 500.0f, h / 500.0f), DirectX::SpriteEffects_None, 0.0f);

}

void SpriteBase::getXY(float &x, float &y) {
	x = X;
	y = Y;
}
void SpriteBase::setXY(float x, float y) {
	X = x;
	Y = y;
}