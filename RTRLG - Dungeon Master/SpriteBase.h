#pragma once

#include "SpriteBatch.h"
#include "DDSTextureLoader.h"
#include "TextureDatabase.h"
#include "defines.h"

#include <D3D11.h>
#include <windef.h>

using namespace DirectX;
using namespace std;
using namespace Windows::Foundation;
using namespace Microsoft::WRL;

class SpriteBase {
public:
	SpriteBase();

	SpriteBase(float x, float y);

	void Draw(SpriteBatch* sb, Windows::Foundation::Rect wb);
	void setTexture(TextureDatabase* db, string text_name);

	void getXY(float &x, float &y);
	void setXY(float x, float y);

private:
	float X;
	float Y;

	ComPtr<ID3D11ShaderResourceView> m_Texture;
};