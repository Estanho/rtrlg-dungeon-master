#pragma once
#include "Direct3DBase.h"
#include <DirectXMath.h>

#include "SpriteBatch.h"
#include "SpriteFont.h"

using namespace DirectX;

class GameFont {
private:
	XMFLOAT2 pos;

public:
	SpriteFont* spritefont;
	const wchar_t* text;

	GameFont(const wchar_t* t, SpriteFont* s, XMFLOAT2 p);
	~GameFont();

	void posxy(float X, float Y);
	XMFLOAT2 posxy();
};

