#pragma once
#include "SpriteBase.h"

class MapTile //N�o herda de SpriteBase para economizar mem�ria!
{
private:
	bool collide;
	bool visible;

	ComPtr<ID3D11ShaderResourceView> m_Texture;

public:
	MapTile(void);
	~MapTile(void);

	void Draw(SpriteBatch* sb, float x, float y, Windows::Foundation::Rect wb);
	void setTexture(TextureDatabase* db, string text_name);

	inline void setVisible(bool opt) {
		visible = opt;
	}
	inline void setCollide(bool opt) {
		collide = opt;
	}
};

