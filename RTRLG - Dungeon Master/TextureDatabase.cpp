#include "pch.h"
#include "TextureDatabase.h"
#include "DirectXHelper.h"

TextureDatabase::TextureDatabase(ID3D11Device* d3ddev) {
	d3dDev = d3ddev;
}

TextureDatabase::~TextureDatabase(void)
{
}

void TextureDatabase::loadTexture(string name, const wchar_t* filename) {
	ComPtr<ID3D11ShaderResourceView> m_Texture = nullptr;

	HRESULT hr = CreateDDSTextureFromFile(d3dDev, filename, nullptr, &m_Texture, MAXSIZE_T);
	DX::ThrowIfFailed(hr);

	m_textures.insert(pair< string, ComPtr<ID3D11ShaderResourceView> >(name, m_Texture));
}

ComPtr<ID3D11ShaderResourceView> TextureDatabase::getTexture(string name) {
	return m_textures.find(name)->second;
}

void TextureDatabase::loadGameTextures() {
	loadTexture("floor", L"Assets/floor.dds");
}