#pragma once
#include <string>
#include <map>

#include <D3D11.h>
#include <windef.h>

#include "SpriteBatch.h"
#include "DDSTextureLoader.h"

using namespace DirectX;
using namespace std;
using namespace Windows::Foundation;
using namespace Microsoft::WRL;

class TextureDatabase
{
private:
	map< string, ComPtr<ID3D11ShaderResourceView> > m_textures;

public:
	TextureDatabase(ID3D11Device*);
	~TextureDatabase(void);
	
	ID3D11Device* d3dDev;

	void loadTexture(string, const wchar_t* text_name);
	ComPtr<ID3D11ShaderResourceView> getTexture(string name);

	void loadGameTextures();
	void loadMenuTextures();
};

