#include "pch.h"
#include "MapTile.h"
#include <D3D11.h>
#include <windef.h>

using namespace Windows::Graphics::Display;
using namespace DirectX;
using namespace Microsoft::WRL;

MapTile::MapTile(void) : collide(false), visible(false) {
	m_Texture = nullptr;
}


MapTile::~MapTile(void)
{
}

void MapTile::Draw(SpriteBatch* sb, float x, float y, Windows::Foundation::Rect wb) {
	if(visible) {
		float scale = DisplayProperties::LogicalDpi;
		float diameter = wb.Width * scale;
		float h = wb.Height * scale;
		sb->Draw(m_Texture.Get(), XMFLOAT2(x, y), nullptr, Colors::White, 0.0f, XMFLOAT2(x,y), XMFLOAT2(diameter / 500.0f, h / 500.0f), DirectX::SpriteEffects_None, 0.0f);

	}
}

void MapTile::setTexture(TextureDatabase* db, string text_name) {
	m_Texture = db->getTexture(text_name);
}