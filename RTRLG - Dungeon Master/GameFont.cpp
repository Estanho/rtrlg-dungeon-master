#include "pch.h"
#include "GameFont.h"


GameFont::GameFont(const wchar_t* t, SpriteFont* s, XMFLOAT2 p) {
	text = t;
	spritefont = s;
	pos = p;
}
GameFont::~GameFont() {
	delete text;
}
void GameFont::posxy(float X, float Y) {
	pos.x = X;
	pos.y  = Y;
}

XMFLOAT2 GameFont::posxy() {
	return pos;
}
