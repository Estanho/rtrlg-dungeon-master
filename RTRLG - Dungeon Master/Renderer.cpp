﻿#include "pch.h"
#include "Renderer.h"

#include <string.h>
#include <sstream>

using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::UI::Core;
using namespace Windows::Graphics::Display;

Renderer::Renderer() :
	m_loadingComplete(false),
	m_indexCount(0)
{
	scale = DisplayProperties::LogicalDpi / 96.0f;
	m_loadingComplete2 = false;

	upsideLeftPosX = 0.0f;
	upsideLeftPosY = 0.0f;
}

bool Renderer::load() {
	arialFont16 = unique_ptr<SpriteFont>( new SpriteFont(m_d3dDevice.Get(), L"Assets\\Fonts\\Arial.spritefont"));
	if(!arialFont16.get()) return false;
	arialFont50 = unique_ptr<SpriteFont>( new SpriteFont(m_d3dDevice.Get(), L"Assets\\Fonts\\Arial50.spritefont"));
	if(!arialFont50.get()) return false;
	timesFont16 = unique_ptr<SpriteFont>( new SpriteFont(m_d3dDevice.Get(), L"Assets\\Fonts\\Times.spritefont"));
	if(!timesFont16.get()) return false;

	m_spriteBatch = unique_ptr<SpriteBatch>(new DirectX::SpriteBatch(m_d3dContext.Get()));
	if(!m_spriteBatch.get()) return false;

	m_textures = new TextureDatabase(m_d3dDevice.Get());
	m_textures->loadGameTextures();

	for(int i = 0; i < 500; i++) {
		for(int j = 0; j < 500; j++) {
			gameMap[i][j].setTexture(m_textures, "floor");
			gameMap[i][j].setVisible(true);
		}
	}
	
	return true;
}

void Renderer::CreateDeviceResources()
{
	Direct3DBase::CreateDeviceResources();

	if(!load()) throw;
}

void Renderer::CreateWindowSizeDependentResources()
{
	Direct3DBase::CreateWindowSizeDependentResources();

	/*sprites.push_back(new SpriteBase(0.0f, 0.0f));
	sprites.back()->setTexture(m_textures, "floor");
	sprites.push_back(new SpriteBase(59.0f, 80.0f));
	sprites.back()->setTexture(m_textures, "floor");
	sprites.push_back(new SpriteBase(100.0f, 100.0f));
	sprites.back()->setTexture(m_textures, "floor");
	sprites.push_back(new SpriteBase(300.0f, 300.0f));
	sprites.back()->setTexture(m_textures, "floor");*/

	texts.push_back(new GameFont(L"Hello World", arialFont50.get(), XMFLOAT2(50.0f, 32.0f)) );
	m_loadingComplete = true;
}

void Renderer::Update(float timeTotal, float timeDelta) {

}

void Renderer::Render()
{
	float midnightBlue[] = { 0.098f, 0.098f, 0.439f, 1.000f };
	m_d3dContext->ClearRenderTargetView(
		m_renderTargetView.Get(),
		midnightBlue
		);

	m_d3dContext->ClearDepthStencilView(
		m_depthStencilView.Get(),
		D3D11_CLEAR_DEPTH,
		1.0f,
		0
		);

	// Espera o load acontecer
	if (!m_loadingComplete)
	{
		return;
	}

	m_spriteBatch->Begin();

	int i, jinit;
	int maxi, maxj;
	i = static_cast <int> (std::floor(upsideLeftPosX/32.0f));
	jinit = static_cast <int> (std::floor(upsideLeftPosY/32.0f));

	maxi = i + static_cast <int>(std::ceil(m_windowBounds.Width/32.0f));
	maxj = jinit + static_cast <int>(std::ceil(m_windowBounds.Height/32.0f));

	for(; i < maxi; ++i) {
		for(int j = jinit; j < maxj; ++j) {
			
			ostringstream sstream;
			/*sstream << "i: " << i << " j: " << j << endl;
			sstream << "i: " << (float)i*32.0f - upsideLeftPosX << " j: " << (float)j*32.0f - upsideLeftPosY << endl;
			sstream << "maxi: " << maxi << " maxj: " << maxj << endl;
			//sstream << "maxi: " << maxi << " maxj: " << maxj << endl;
			string s = sstream.str();
			OutputDebugStringA(s.c_str());*/
			
			gameMap[i][j].Draw(m_spriteBatch.get(), (float)i*32.0f - upsideLeftPosX, (float)j*32.0f - upsideLeftPosY, m_windowBounds);
		}
	}

	for(auto it = sprites.begin(); it != sprites.end(); ++it) {
		(*it)->Draw(m_spriteBatch.get(), m_windowBounds);
	}
	
	for(auto it = texts.begin(); it != texts.end(); ++it) {
		(*it)->spritefont->DrawString(m_spriteBatch.get(), (*it)->text , (*it)->posxy());//, Colors::White, 0.0f, XMFLOAT2(250.0f, 250.0f), XMFLOAT2(diameter / 500.0f, h / 500.0f), DirectX::SpriteEffects_None, 0.0f);
	}

	m_spriteBatch->End();

	m_d3dContext->OMSetRenderTargets(
		1,
		m_renderTargetView.GetAddressOf(),
		m_depthStencilView.Get()
		);
}